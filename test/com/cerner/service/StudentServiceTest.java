package com.cerner.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.cerner.dao.IDBConnection;
import com.cerner.dao.StudentDao;
@RunWith(MockitoJUnitRunner.class)
public class StudentServiceTest {
    
	@Mock
    private DataSource ds;
    
    @Mock
    private Connection c;
    
    @Mock
    private PreparedStatement mockStmt;
    
    @Mock
    private ResultSet rs;

    private StudentDao mockStudentDao;
    @Before
    public void setUp() throws Exception {
       
    	 Mockito.when(c.prepareStatement(Mockito.anyString())).thenReturn(mockStmt);
         Mockito.when(ds.getConnection()).thenReturn(c); 
         Mockito.when(mockStmt.executeQuery()).thenReturn(rs);
    	IDBConnection con=Mockito.mock(IDBConnection.class);
        Mockito.when(con.getConnection()).thenReturn(c);
        mockStudentDao = new StudentDao(con);
        
    }
    @Test
	public void testGetAllRecordsForResultsetNull() {
		Mockito.when(mockStudentDao.ReadAll()).thenReturn(rs);
		//StudentService.setObStudentDao(mockStudentDao);
		String resultSet = StudentService.GetAllRecords();
		Assert.assertEquals(null, resultSet);
		
	}
  
	@Test
	public void testGetAllRecordsForResultsetNotNull() {
		String rs = "[]";
	    
		Mockito.when(mockStudentDao.ReadAll()).thenReturn(Mockito.mock(ResultSet.class));

		String resultSet = StudentService.GetAllRecords();
		Assert.assertEquals(rs, resultSet);
		} 
	
	@Test
	public void testGetRecordByIdForResultsetNull() {
		Mockito.when(mockStudentDao.ReadAll()).thenReturn(null);
		
		String resultSet = StudentService.GetAllRecords();
		Assert.assertEquals(null, resultSet);
		
	}
  
	@Test
	public void testGetRecordByIdForResultsetNotNull() {
		String rs = "[]";
	    
		Mockito.when(mockStudentDao.ReadAll()).thenReturn(Mockito.mock(ResultSet.class));

		String resultSet = StudentService.GetAllRecords();
		Assert.assertEquals(rs, resultSet);
		
	}
}