package com.cerner.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
@RunWith(MockitoJUnitRunner.class)
public class StudentDaoTest {
    
	@Mock
    private DataSource ds;
    
    @Mock
    private Connection c;
    
    @Mock
    private PreparedStatement mockStmt;
    
    @Mock
    private ResultSet rs;

    @Before
    public void setUp() throws Exception {
        Assert.assertNotNull(ds);
        Mockito.when(c.prepareStatement(Mockito.anyString())).thenReturn(mockStmt);
        Mockito.when(ds.getConnection()).thenReturn(c); 
        Mockito.when(mockStmt.executeQuery()).thenReturn(rs);
        
    }

    
    @Test
    public void CreateStudentInfo() throws SQLException, ClassNotFoundException {
    	c.setAutoCommit(false);
    	IDBConnection con=Mockito.mock(IDBConnection.class);
        Mockito.when(con.getConnection()).thenReturn(c);
    	
	    	int id=2000;
	        String name="soj";
	        int age=23;
	        StudentDao daoInstance = new StudentDao(con);
	        boolean actualres=daoInstance.CreateStudent(id,name,age);
	        boolean expectedres=true;
	        Assert.assertEquals(expectedres,actualres);
	        
       
    }
   
    @Test
    public void UpdateStudentInfo() throws SQLException, ClassNotFoundException {
    	c.setAutoCommit(false);
    	IDBConnection con=Mockito.mock(IDBConnection.class);
        Mockito.when(con.getConnection()).thenReturn(c);
    	
	    	int id=2000;
	        String name="soj";
	        int age=23;
	        StudentDao daoInstance = new StudentDao(con);
	        boolean actualres=daoInstance.UpdateStudent(id,name,age);
	        boolean expectedres=true;
	        Assert.assertEquals(expectedres,actualres);
	        
       
    }

    @Test
    public void DeleteStudentRecordById() throws SQLException, ClassNotFoundException {
    	c.setAutoCommit(false);
    	IDBConnection con=Mockito.mock(IDBConnection.class);
        Mockito.when(con.getConnection()).thenReturn(c);
	    	int id=2000;
	        StudentDao daoInstance = new StudentDao(con);
	        boolean actualres=daoInstance.DeleteRecordById(id);
	        boolean expectedres=true;
	        Assert.assertEquals(expectedres,actualres);
    }
    
    @Test
    public void DeleteStudentRecordByIdFail() throws SQLException, ClassNotFoundException {
    	c.setAutoCommit(false);
    	IDBConnection con=Mockito.mock(IDBConnection.class);
        Mockito.when(con.getConnection()).thenReturn(c);
	    	int id=-2000;
	        StudentDao daoInstance = new StudentDao(con);
	        boolean actualres=daoInstance.DeleteRecordById(id);
	        boolean expectedres=true;
	        Assert.assertEquals(expectedres,actualres);
    }
    
    @Test
    public void ReadStudentRecordById() throws SQLException, ClassNotFoundException {
	    	int id=3;
	    	StudentDao daoInstance = new StudentDao(new DBConnection());
	        ResultSet rs=daoInstance.GetRecordById(id);  
	        rs.first();
	        int actualres=rs.getInt(1);
	        Assert.assertEquals(id,actualres);
    }
    
    @Test
    public void ReadAllStudentRecords() throws SQLException, ClassNotFoundException {
	    	StudentDao daoInstance = new StudentDao(new DBConnection());
	        ResultSet rs=daoInstance.ReadAll();
	        while(rs.next())
	        	Assert.assertNotNull(rs);
    }
    
    
   @Test
   public void CreateStudentRecordFail() throws SQLException, ClassNotFoundException {
	   int id=100;
       String name="soj";
       int age=23;
       StudentDao daoInstance = new StudentDao(new DBConnection());
	   boolean actualres=daoInstance.CreateStudent(id,name,age);
       boolean expectedres=false;
       Assert.assertEquals(expectedres,actualres);
}
    
    
    

}
