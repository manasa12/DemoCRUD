package com.cerner.controller;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.cerner.dao.IDBConnection;
import com.cerner.dao.StudentDao;
import com.cerner.service.StudentService;


public class UpdateWebServiceTest {

	@Mock
    private Connection c;
    
    @Mock
    private PreparedStatement mockStmt;
    
    @Mock
    private ResultSet rs;
    
private UpdateWebService updateWebService = null;
    
    private StudentDao mockStudentDao; 
   
	@Before
	public void setUp() throws Exception {
		updateWebService = new UpdateWebService();
		
		IDBConnection con=Mockito.mock(IDBConnection.class);
        Mockito.when(con.getConnection()).thenReturn(c);
        mockStudentDao = new StudentDao(con);	
	}

	@Test
	public void testUpdateStudent_studentInfoNull() {
	 Response observedStatus = updateWebService.UpdateStudent(null);
		int status= observedStatus.getStatus();
		assertEquals(400,status);
	}
   
	@Test
	public void testUpdateStudent_studentUpdatedNormally() throws JSONException {
		JSONObject jo=createJSON();
		Response observedStatus = updateWebService.UpdateStudent(jo.toString());
		int status= observedStatus.getStatus();
		assertEquals(200,status);
	}

		public JSONObject createJSON() throws JSONException 
		{
			String [] Colnames = {"Sid","StudentName","Age"};
			String [] values = {"10", "madhu","30"};
			
			JSONObject obj = new JSONObject();
			for(int i=0;i<3;i++)
			{
				String key = Colnames[i];
				String val = values[i];
				obj.put(key, val);
			}
			return obj;
			
			
		}   


}
