package com.cerner.controller;

import static org.junit.Assert.*;

import static org.mockito.Mockito.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.cerner.dao.IDBConnection;
import com.cerner.dao.StudentDao;
import com.cerner.service.StudentService;

public class ReadWebServiceTest {

	@Mock
    private Connection c;
    
    @Mock
    private PreparedStatement mockStmt;
    
    @Mock
    private ResultSet rs;
    
	private ReadWebService readWebService=null;

	private StudentDao mockStudentDao;
   
	@Before
	public void setUp() throws Exception {
		readWebService = new ReadWebService();
	
		IDBConnection con=Mockito.mock(IDBConnection.class);
        Mockito.when(con.getConnection()).thenReturn(c);
        
        mockStudentDao = Mockito.mock(StudentDao.class);
        when(mockStudentDao.getDbConnection()).thenReturn(con);
	}

	@Test
	public void testGetAllRecords_recordsOk() {
		
		 
		when(mockStudentDao.ReadAll()).thenReturn(rs);
	
		Response observedStatus = readWebService.GetAllRecords();
		int status= observedStatus.getStatus();
		  assertEquals(200,status);
		
	}
	
	@Test
	public void testGetAllRecords_recordsNull() {
		
		//when(mockStudentDao.ReadAll()).thenReturn(null);
		Response observedStatus = readWebService.GetAllRecords();
		int status= observedStatus.getStatus();
		  assertEquals(404,status);
		
	}
	
	@Test
	public void testGetRecordById_nullStudentId() {
		Response observedStatus = readWebService.GetRecordById(null);
		int status= observedStatus.getStatus();
		  assertEquals(400,status);
	}
    
	@Test
	public void testGetRecordById_nullDataFromDatabase() {
		
		when(mockStudentDao.GetRecordById(100)).thenReturn(null);
		Response observedStatus = readWebService.GetRecordById("100");
		int status= observedStatus.getStatus();
		  assertEquals(404,status);
	}
	
	@Test
	public void testGetRecordsById_recordsOk() {
		
		when(mockStudentDao.GetRecordById(1)).thenReturn(rs);
		Response observedStatus = readWebService.GetRecordById("2");
		int status= observedStatus.getStatus();
		  assertEquals(200,status);
	
	}
	
	
}
