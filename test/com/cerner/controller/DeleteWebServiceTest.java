package com.cerner.controller;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.cerner.dao.IDBConnection;
import com.cerner.dao.StudentDao;
import com.cerner.service.StudentService;

public class DeleteWebServiceTest {

	@Mock
    private Connection c;
    
    @Mock
    private PreparedStatement mockStmt;
    
    @Mock
    private ResultSet rs;
	 private DeleteWebService deleteWebService = null;
     
     private StudentDao mockStudentDao; 
	@Before
	public void setUp() throws Exception {
		deleteWebService = new DeleteWebService();
		
		IDBConnection con=Mockito.mock(IDBConnection.class);
        Mockito.when(con.getConnection()).thenReturn(c);
        mockStudentDao = Mockito.mock(StudentDao.class);
        when(mockStudentDao.getDbConnection()).thenReturn(con);
       
	}
     
	@Test
	public void testDeleteRecordById_nullStudentId() {
		Response observedStatus = deleteWebService.DeleteRecordById(null);
		int status= observedStatus.getStatus();
		  assertEquals(400,status);
	}
	
	@Test
	public void testDeleteRecordById_recordDeleted() {
		//when(mockStudentDao.DeleteRecordById(1)).thenReturn(true);
	
		Response observedStatus = deleteWebService.DeleteRecordById("1");
		int status= observedStatus.getStatus();
		  assertEquals(200,status);
	
	}

	@Test
	public void testDeleteRecordById_recordDeletionProblem() {	
		//when(mockStudentDao.DeleteRecordById(1)).thenReturn(false);
		
		Response observedStatus = deleteWebService.DeleteRecordById("1");
		int status= observedStatus.getStatus();
		  assertEquals(400,status);
		
	}

}
