package com.cerner.utils;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.cerner.dao.DBConnection;
import com.cerner.dao.StudentDao;

public class ResultSetToJSONTest {

	@Test
	public void testConvertJSON() throws SQLException {
		int i=0;
		boolean js=true;
		List<JSONObject> resList = new ArrayList<JSONObject>();
	ResultSet res=CreateandReadStudentInfo();
		
	resList=ResultSetToJSON.convertJSON(res);
	while (i < resList.size()) {
		if(!(resList.get(i) instanceof JSONObject))
	   	js=false;	
		i++;
	}
	Assert.assertEquals(true,js);
	}

	@Test(expected=Exception.class)
	public void testConvertJSONException() throws SQLException {
		
		List<JSONObject> resList = new ArrayList<JSONObject>();
		ResultSet res=null;
	resList=ResultSetToJSON.convertJSON(res);
	}
	
	public ResultSet CreateandReadStudentInfo() throws SQLException
	{
		 int id = 0;
	       
	       StudentDao daoInstance = new StudentDao(new DBConnection());
	        ResultSet readAll=daoInstance.ReadAll();
	        if(readAll!=null)
	        {
	        	readAll.first();
	        	id=readAll.getInt(1);
	        }
		   ResultSet rs=daoInstance.GetRecordById(id);  
	        return rs;
	}
	
}
