/**
 * 
 */
package com.cerner.dao;

import java.sql.SQLException;

import java.sql.Connection;

/**
 * @author MN051791
 *
 */
public interface IDBConnection 
{
	/**
	 * getConnection
	 * @return the <code>Connection</code> Instance representing the SQL connection.
	 * @throws SQLException
	 */
	Connection getConnection() throws ClassNotFoundException, SQLException;
}
