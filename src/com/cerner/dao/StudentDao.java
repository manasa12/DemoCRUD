package com.cerner.dao;

import com.cerner.dao.DBConnection;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;



public class StudentDao
{
	IDBConnection dbCon = null;
	PreparedStatement preparedStatement=null;
	ResultSet resultSet=null;

	public StudentDao(IDBConnection connection)
	{
		this.dbCon = connection;
	}
	
	public IDBConnection getDbConnection()
	{
		return (dbCon);
	}
	
	public ResultSet ReadAll()
	{
		try
		{
			String sql="Select * from studentinfo";			
			preparedStatement = (PreparedStatement) getDbConnection().getConnection().prepareStatement(sql);
			resultSet = preparedStatement.executeQuery();
			if(!resultSet.next())
				return null;
		}
		catch(Exception e)
		{
			System.out.println("Error in Reading");
			e.printStackTrace();
		}
		return resultSet;
	}
	
	
	public ResultSet GetRecordById(int StudentId)
	{
		try{
			String sql="Select * from studentinfo where Sid= ?";			
			
			preparedStatement = (PreparedStatement) getDbConnection().getConnection().prepareStatement(sql);
			preparedStatement.setInt(1,StudentId);
			resultSet = preparedStatement.executeQuery();
		if(!resultSet.next())
		return null; 
			//System.out.print(resultSet.getString("StudentName"));
		}
		
		catch(Exception e)
		{
			System.out.println("Error in GetrecordById()");
			e.printStackTrace();
		}
				return resultSet;
		} 
			
	

	
	
	public boolean UpdateStudent(int Sid,String StudentName,int Age)
	{
		try{			
			String query = "update studentinfo set StudentName=?,Age=? where Sid = ?";
		    preparedStatement = (PreparedStatement) getDbConnection().getConnection().prepareStatement(query);
		    preparedStatement.setString(1,StudentName);
		    preparedStatement.setInt(2, Age);
		    preparedStatement.setInt(3, Sid);	
		   
		    preparedStatement.executeUpdate();
			return true;
		}
		catch(Exception e)
		{
			System.out.println("Error in updateStudents");
			e.printStackTrace();
			return false;
		}		
	}
	
	
	public boolean CreateStudent(int Sid,String StudentName,int Age)
	{
		
		try
		{
			String query = "Insert into studentinfo values(?,?,?)";
			preparedStatement = (PreparedStatement) getDbConnection().getConnection().prepareStatement(query);
		    preparedStatement.setInt(1,Sid);
		    preparedStatement.setString(2, StudentName);	
		    preparedStatement.setInt(3, Age);
		    preparedStatement.executeUpdate();
		    return true;
		}
		catch(Exception e)
		{
			System.out.println("Error in CreateStudents");
			e.printStackTrace();
			return false;
		}		
	}
	
	public boolean DeleteRecordById(int StudentId)
	{
		try
		{
			String sql="Delete from studentinfo where Sid= ?";				
			preparedStatement = (PreparedStatement) getDbConnection().getConnection().prepareStatement(sql);
			preparedStatement.setInt(1,StudentId);
			 preparedStatement.executeUpdate();
			 return true;
		}
		catch(Exception e)
		{
			System.out.println("Error in DeleteRecordById()");
			e.printStackTrace();
			return false;
		}
	}
}

