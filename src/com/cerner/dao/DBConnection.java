package com.cerner.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static com.cerner.constants.DBConstants.DB_CLASS;
import static com.cerner.constants.DBConstants.DB_URL;
import static com.cerner.constants.DBConstants.USER;
import static com.cerner.constants.DBConstants.PWD;

public class DBConnection implements IDBConnection
{
	@Override
	public Connection getConnection() throws ClassNotFoundException, SQLException
	{
    	Class.forName(DB_CLASS);
    	//log4j    	
	    System.out.println("Driver loaded sucessfully");
	    return (DriverManager.getConnection(DB_URL,USER,PWD));
	}
}
