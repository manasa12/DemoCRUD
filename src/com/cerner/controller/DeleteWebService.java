package com.cerner.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.cerner.service.StudentService;


@Path("/delete")
public class DeleteWebService {

	  @GET
	    @Produces(MediaType.APPLICATION_JSON)
	    @Path("/{StudentId}")
public static Response DeleteRecordById(@PathParam("StudentId") String StudentId)
{
		  if(StudentId == null)
		   {
			return Response.status(Status.BAD_REQUEST).build();
		   }
	   boolean deleted;
	   
	deleted=StudentService.DeleteRecordById(StudentId);
	if(deleted==true)
	return Response.status(Status.OK).build();
	else
		return Response.status(Status.BAD_REQUEST).build();
}
}
