package com.cerner.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.cerner.service.StudentService;

@Path("/update")
public class UpdateWebService {

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response UpdateStudent(String jsonString)
    {
    	boolean updated;
    	if(jsonString == null)
    	{
    		return Response.status(Status.BAD_REQUEST).build();
    	}
    	else
    	{
    	updated=StudentService.UpdateStudent(jsonString);
    	if(updated==true)
    		return Response.ok().entity(jsonString).build();
    	else
    		return Response.status(Status.BAD_REQUEST).build();
    	}
    }
}
