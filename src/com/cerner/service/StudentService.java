package com.cerner.service;

import java.sql.ResultSet;
import java.util.List;

import org.json.JSONObject;

import com.cerner.dao.DBConnection;
import com.cerner.dao.StudentDao;
import com.cerner.utils.ResultSetToJSON;
import org.json.JSONException;
public class StudentService
{
	
	static StudentDao obStudentDao = new StudentDao(new DBConnection());

	public static String GetAllRecords() 
	 {
	
		 ResultSet resultSet=null;
         
         resultSet = obStudentDao.ReadAll();
         if(resultSet==null)
         {
        	 return null;
         }
         else{
         List<JSONObject> resList = ResultSetToJSON.convertJSON(resultSet);    		
         
         return resList.toString();
         }
        }
	 
	 public static String GetRecordById(String StudentId) 
	 {
		 
		  ResultSet resultSet=null;
          
          resultSet = obStudentDao.GetRecordById(Integer.parseInt(StudentId));
          if(resultSet==null)
          {
         	 return null;
          }
          else{
 			List<JSONObject> resList = ResultSetToJSON.convertJSON(resultSet);
 			return resList.toString();
		
          }
	 }
	 
	 public static boolean CreateStudent(String jsonString)
	 {
		 
		 try {
			 
//			 jsonParse.parseJSONToCreate(jsonString);
			 int Sid; String StudentName; int Age;
				JSONObject obj = new JSONObject(jsonString);
				Sid = (Integer.parseInt(obj.getString("Sid")));
				StudentName=obj.getString("StudentName");
				Age = (Integer.parseInt(obj.getString("Age")));
				StudentDao oStudentDao = new StudentDao(new DBConnection());
				boolean created=oStudentDao.CreateStudent(Sid, StudentName, Age);
				return created;
	           }
	           catch (JSONException e) {
				e.printStackTrace();
				System.out.println("Error while calling parseJSONToCreate()");
				return false;
			   }
	 }
	 
	 public static boolean UpdateStudent(String jsonString)
	 {
		 try {
			 
//			 jsonParse.parseJSONToUpdate(jsonString);
			 int Sid; String StudentName; int Age;
				JSONObject obj = new JSONObject(jsonString);
				Sid = (Integer.parseInt(obj.getString("Sid")));
				StudentName=obj.getString("StudentName");
				Age = (Integer.parseInt(obj.getString("Age")));
				StudentDao oStudentDao = new StudentDao(new DBConnection());
				boolean updated=oStudentDao.UpdateStudent(Sid,StudentName,Age);
				return updated;
	           }
	           catch (JSONException e) {
				e.printStackTrace();
				System.out.println("Error while calling parseJSONToUpdate()");
				return false;
			   }
	 }
	 
	 public static boolean DeleteRecordById(String StudentId)
	 {  
		 boolean deleted;
		 
		 deleted=obStudentDao.DeleteRecordById(Integer.parseInt(StudentId));
		 if(deleted==true)
			 return true;
		 else
			 return false;
		
	 }
}
